#pragma once
#include <string>
#include <vector>
#include <logger/log.h>

extern class LogManager : public std::vector<Log*> {

private:

public:
    LogManager() {}
    ~LogManager() {
        clearData();
    }

    /*
    A�ade un Log al vector de la clase.
    */
    inline void addLog(Log* log) {
        push_back(log);
    }
    /*
    Ejecuta un tick del Logger (Env�a los Log existentes a la consola).
    */
    inline void onTick() {
        for (unsigned int i = 0; i < size(); i++) {
            Log* log = (*this)[i];
            if (log) {
                log->printText();
                delete log;
            }
        }
        clear();
    }
    /*
    Borra todos los datos que hay dentro
    */
    inline void clearData() {
        for (unsigned int i = 0; i < size(); i++) {
            delete (*this)[i];
        }
        clear();
    }
} gLog;