#include "map.h"
#include <logger/logger.h>

Map::Map() {
}

Map::~Map() {
    iterator it = begin(),
             final = end();
    while (it != final) {
        delete it->second;
        it++;
    }
    delete _mapText;
    delete _gridText;
    delete _mmapText;
    delete _mapSprite;
    delete _gridSprite;
    delete _miniMapSprite;
}

void Map::init() {
    _mapText = new sf::RenderTexture();
    _gridText = new sf::RenderTexture();
    _mmapText = new sf::RenderTexture();
    _mapSprite = new sf::Sprite();
    _gridSprite = new sf::Sprite();
    _miniMapSprite = new sf::Sprite();
    fillMap();
}

sf::RenderTexture * Map::getImg() {
    return _mapText;
}

void Map::createGrid(std::pair<unsigned int, unsigned int> pos) {
    if ((*this)[pos]) {
        _ERRORLOG("Intentando crear Grid ya existente: " << pos.first << "," << pos.second);
        return;
    }
    Grid *grid = new Grid(pos);
    (*this)[pos] = grid;
}

void Map::fillMap(){
    uint32_t countX = 40;
    uint32_t countY = 40;
    uint32_t pixelsX = (uint32_t)((countX * 44) * 1.5f);
    uint32_t pixelxY = (uint32_t)((countY * 44) * 1.5f);
    _mapText->create(pixelsX, pixelxY);
    _gridText->create(pixelsX, pixelxY);
    _mmapText->create(pixelsX, pixelxY);
    for (uint32_t x = 1; x <= countX; x++){
        for (uint32_t y = 1; y <= countY; y++){
            createGrid(std::pair<uint32_t, uint32_t>(x, y));
        }
    }
}

void Map::paint(sf::RenderWindow *window) {
    //unsigned int countX = _mapText->getSize().x / 44;
    //unsigned int countY = _mapText->getSize().y / 44;
    iterator it = begin(),
             final = end();
    int count = 0;

    sf::View view = _mapText->getView();
    sf::Vector2f center(1, (float)(_mapText->getSize().y / 2));
    view.setCenter(center);
    _mapText->setView(view);
    
    sf::View gView = _gridText->getView();
    gView.setCenter(center);
    _gridText->setView(gView);

    //unsigned int size = 200;
    //sf::View mmView(sf::FloatRect(window->getView().getCenter().x, window->getView().getCenter().y, size, window->getSize().y*size / window->getSize().x));
    sf::View mmView(_mmapText->getView());
    mmView.setViewport(sf::FloatRect(1.f - (1.f*mmView.getSize().x) / window->getSize().x - 0.02f,
                      0.02f,
                      (1.f*mmView.getSize().x) / window->getSize().x,
                      (1.f*mmView.getSize().y) / window->getSize().y));
    mmView.zoom(4.f);

    //sf::View mmView(window->getView());    //TODO a�adir MiniMap
    //mmView.setSize(sf::Vector2f(1920, 1080));
    //mmView.setViewport(sf::FloatRect(0.8f, 0.0f, 0.2f, 0.2f));
    //mmView.setViewport(sf::FloatRect(0.8f, 0, 1, 1));
    //mmView.zoom(0.4f);
    _mmapText->setView(mmView);

    while (it != final) {
        Grid *grid = static_cast<Grid*>(it->second);
        if (!grid) {
            _ERRORLOG("No hay Grid " << it->first.first << ", " << it->first.second);
            continue;
        }
        grid->drawTile(_mapText);
        grid->drawTile(_mmapText);
        grid->drawGrid(_gridText);
        it++;
        count++;
    }

    _mapText->display();
    _gridText->display();
    _mmapText->display();

    _mapSprite->setTexture(_mapText->getTexture());
    _gridSprite->setTexture(_gridText->getTexture());
    _miniMapSprite->setTexture(_mmapText->getTexture());
}

void Map::reset() {
    _mapText->clear();
    _gridText->clear();
    _mmapText->clear();
}

sf::Sprite *Map::getMapSprite() {
    return _mapSprite;
}

sf::Sprite * Map::getGridSprite(){
    return _gridSprite;
}

sf::Sprite * Map::getMiniMapSprite(){
    return _miniMapSprite;
}
