#pragma once

#include <map>

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include "grid.h"

extern class Map : public std::map<std::pair<unsigned int, unsigned int>, Grid*> {
private:
   // Texturas de las distintas partes que se visualizan del mapa.
    sf::RenderTexture *_mapText;                         // RenderTexture con la imagen completa del mapa.
    sf::RenderTexture *_gridText;                        // RenderTexture con el grid visual de todo el mapa.
    sf::RenderTexture *_mmapText;                        // RenderTexture con el grid visual de todo el mapa.

   // Sprites para visualizar las texturas del mapa.
    sf::Sprite *_mapSprite;
    sf::Sprite *_gridSprite;
    sf::Sprite *_miniMapSprite;

    sf::RenderWindow *_gameWindow;   // Puntero al RenderWindow principal.

public:
   Map();
    ~Map();
   void init();
    sf::RenderTexture *getImg();
    void createGrid(std::pair<unsigned int, unsigned int> pos);
    void fillMap();
    void paint(sf::RenderWindow *window);
    void reset();
    sf::Sprite *getMapSprite();
    sf::Sprite *getGridSprite();
    sf::Sprite *getMiniMapSprite();
} gMap;