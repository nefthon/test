#include "grid.h"
#include <SFML/Graphics/Texture.hpp>
#include <logger/logger.h>

sf::Vector2f getPixelCoords(unsigned int x, unsigned int y){
    sf::Vector2i v;
    v.x = x * 44;
    v.y = y * 44;
    return sf::Vector2f((float)((v.x - v.y) / 2), (float)((v.x + v.y) / 2));
}

Grid::Grid(std::pair<unsigned int, unsigned int> pos) {
    _pos = pos;
    Object *tile = new Object();
    static sf::Texture text;
    if (!text.loadFromFile("../media/0x177D.bmp")){
        _ERRORLOG("No se puede cargar la textura");
    }
    tile->setTexture(text);
    if (pos.second == 1)
        tile->setColor(sf::Color::Magenta);
    _mapPos.x = pos.first * 44;
    _mapPos.y = pos.second * 44;
    _screenPos.x = (float)((_mapPos.x - _mapPos.y) / 2);
    _screenPos.y = (float)((_mapPos.x + _mapPos.y) / 2);
    addTile(tile);
}

Grid::~Grid() {
}

void Grid::drawTile(sf::RenderTexture *img) {
    for (int i = 0; i < size(); i++) {
        Object *objectAt = (*this)[i];
        if (objectAt) {
            sf::Vector2f pos = getPixelCoords(_pos.first, _pos.second);
            objectAt->setPosition(pos);
            img->draw(*objectAt);
        }
    }
}

void Grid::drawGrid(sf::RenderTexture * img){
    sf::Vector2f pos = getPixelCoords(_pos.first, _pos.second);
    sf::Vertex line[5];
    // Punto N
    line[0].position = pos;
    line[0].position.x += 22;
    line[0].color = sf::Color::Red;
    //Punto E
    line[1].position = pos;
    line[1].position.x += 44;
    line[1].position.y += 22;
    line[1].color = sf::Color::Red;
    // Punto S
    line[2].position = pos;
    line[2].position.x += 22;
    line[2].position.y += 44;
    line[2].color = sf::Color::Red;
    // Punto W
    line[3].position = pos;
    line[3].position.y += 22;
    line[3].color = sf::Color::Red;
    // Retorno N
    line[4] = line[0];
    img->draw(line, 5, sf::LineStrip);
}

void Grid::addTile(Object * tile) {
    for (int i = 0; i < size(); i++) {
        Object *objectAt = (*this)[i];
        if (objectAt && objectAt == tile) {
            _ERRORLOG("Intentando a�adir de nuevo un tile en el grid " << _pos.first << ", " << _pos.second);
            return;
        }
    }
    push_back(tile);
}

void Grid::deleteTile(Object * tile) {
    for (int i = 0; i < size(); i++) {
        Object *objectAt = (*this)[i];
        if (objectAt && objectAt == tile) {
            erase(begin() + i);
            return;
        }
    }
}

std::pair<unsigned int, unsigned int> Grid::getMapPos() {
    return _pos;
}

sf::Vector2i Grid::getSize() {
    return sf::Vector2i(44, 44);
}

sf::Vector2f Grid::getScreenPos() {
    return _screenPos;
}

sf::Vector2i Grid::getRealPos() {
    return sf::Vector2i(_pos.first * 44, _pos.second * 44);
}
