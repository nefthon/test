#pragma once
#include <vector>
#include <SFML/System/Vector2.hpp>

#include <SFML/Graphics/RenderTexture.hpp>
#include "../objects/object.h"

class Grid : public std::vector<Object*> {
private:
    std::pair<unsigned int, unsigned int> _pos;
    sf::Vector2i _mapPos;
    sf::Vector2f _screenPos;
public:
    Grid(std::pair<unsigned int, unsigned int> pos);
    ~Grid();

    void drawTile(sf::RenderTexture *img);
    void drawGrid(sf::RenderTexture *img);
    void addTile(Object *tile);
    void deleteTile(Object *tile);

    std::pair<unsigned int, unsigned int> getMapPos();
    sf::Vector2i getSize();
    sf::Vector2f getScreenPos();
    sf::Vector2i getRealPos();
};