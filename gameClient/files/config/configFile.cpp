#include "ConfigFile.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>

#include <logger/logger.h>
#include "configManager.h"
#include "configVal.h"
#include "configKey.h"
using namespace std;

ConfigFile::ConfigFile() {
}

ConfigFile::~ConfigFile() {
}

bool ConfigFile::readAll() {
    string line;
    ifstream file(CONFIGFILE);
    if (file.is_open()) {
        while (getline(file, line)) {
            unsigned int chrCount = 0;
            stringstream key, val;
            bool keyReaded = false;
            bool onlyNums = true;
            while (line.size() > chrCount) { // Leemos todos los caracteres del stringstream
                char chr = line.at(chrCount);// Char actual
                chrCount++;
                if (chr == '=') {   // Si es un = ya se ha leido el Key y hay que pasar al Val
                    keyReaded = true;
                    continue;
                }
                else if ((chr < 0 || chr > 255) // Ignoramos car�cteres 'ilegales' a ojos de 'isdigit()' e 'isalpha()'.
                         || (!isdigit(chr) && !isalpha(chr))) {  // y cualquiera de los restantes que no se corresponda a un n�mero o una letra 'b�sica'.
                    continue;
                }
                if (keyReaded) {    // Si ya se ha leido el '=' se procede a rellenar el valor.
                    if (isalpha(chr))   // Si el car�cter corresponde a una letra hay que tratar el valor como un string.
                        onlyNums = false;
                    val << chr;
                }
                else {              // Si no se ha leido el '=' se procede a rellenar la clave.
                    key << chr;
                }
            }
            ConfigVal v;
            if (onlyNums){
                if (atoi(val.str().c_str()) > INT64_MAX && atoi(val.str().c_str()) < UINT64_MAX){
                    uint64_t res = atoi(val.str().c_str());
                    v = res;
                }
                else{
                    int64_t res = atoi(val.str().c_str());
                    v = res;
                }
            }
            else{   // Hay letras, es un string.
                v = val.str();
            }
            gCfg.addKey(key.str(), v);
        }
        file.close();
        return true;
    }
    else{   //No hay game.cfg?
        return false;
    }
}

bool ConfigFile::writeAll(){
    ofstream file(CONFIGFILE);
    if (!file.is_open())
        return false;
    gCfg.setupDefaults();
    int count = 0;
    for (int i = 0; i < cfgKeys_QTY; i++){
        ConfigVal v = gCfg.getVal(cfgKeys(i));
        if (v.isEmpty()){
            _ERRORLOG("Imposible escribir datos para '" << cfgStrings[i] << "'");
            continue;
        }
        if (!v.getString().empty()){
            file << cfgStrings[i] << " = " << v.getString() << endl;
            count++;
            continue;
        }
        else if (v.getUint() > 0){
            file << cfgStrings[i] << " = " << v.getUint() << endl;
            count++;
            continue;
        }
        else {
            file << cfgStrings[i] << " = " << v.getInt() << endl;
            count++;
            continue;
        }
    }
    file.close();
    if (count != cfgKeys_QTY){
        _ERRORLOG("Escritas " << count << "/" << cfgKeys_QTY << "claves en el archivo cfg");
    }
    return true;
}
