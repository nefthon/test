#pragma once

#define CONFIGFILE "game.cfg"

class ConfigFile {
public:
    ConfigFile();
    ~ConfigFile();
    /**
     * \brief Lee todas las claves del archivo #CONFIGFILE
     * \return false si falla.
     */
    static bool readAll();
    /**
     * \brief Escribe todas las claves al archivo #CONFIGFILE
     * \return false si falla
     */
    static bool writeAll();
};