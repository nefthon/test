
set( gc_config_srcs 
	${CMAKE_CURRENT_LIST_DIR}/configManager.h
	${CMAKE_CURRENT_LIST_DIR}/configManager.cpp
	${CMAKE_CURRENT_LIST_DIR}/configKey.h
	${CMAKE_CURRENT_LIST_DIR}/configKey.cpp
	${CMAKE_CURRENT_LIST_DIR}/configVal.h
	${CMAKE_CURRENT_LIST_DIR}/configVal.cpp
	${CMAKE_CURRENT_LIST_DIR}/configFile.h
	${CMAKE_CURRENT_LIST_DIR}/configFile.cpp)
SOURCE_GROUP (config FILES ${gc_config_srcs})

