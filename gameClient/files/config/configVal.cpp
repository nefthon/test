#include "configVal.h"

ConfigVal::ConfigVal() {
    iVal = 0;
    uiVal = 0;
    strVal.clear();
}

ConfigVal::ConfigVal(ConfigVal &other){
    iVal = other.getInt();
    uiVal = other.getUint();
    strVal = other.getString();
}

ConfigVal::ConfigVal(std::stringstream &sstr){
    int64_t i64Test;
    uint64_t ui64Test;
    std::string strTest;
    if (sstr >> i64Test){
        (*this) = i64Test;
        return;
    }
    else if (sstr >> ui64Test){
        (*this) = ui64Test;
        return;
    }
    else if (sstr >> strTest){
        (*this) = strTest;
        return;
    }
}

ConfigVal::~ConfigVal() {
}

ConfigVal::DataTypes ConfigVal::getDataType(){
    DataTypes dt;
    if (!strVal.empty())
        dt = dtString;
    else if (uiVal > 0)
        dt = dtUint64;
    else
        dt = dtInt64;
    return dt;
}

int64_t ConfigVal::getInt(){
    return iVal;
}

uint64_t ConfigVal::getUint(){
    return uiVal;
}

std::string ConfigVal::getString(){
    return strVal;
}

bool ConfigVal::isEmpty(){
    return (!getInt() && !getUint() && getString().empty());
}

ConfigVal ConfigVal::operator=(ConfigVal & other){
    switch (other.getDataType()){
        case dtInt64:
            iVal = other.getInt();
            break;
        case dtUint64:
            uiVal = other.getUint();
            break;
        case dtString:
            strVal = other.getString();
            break;
        default:
            _ERRORLOG("ConfigVal:DataType sin definir");
            break;
    }
    return *this;
}
