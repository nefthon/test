#pragma once
#include <string>
#include <sstream>
#include <logger/logger.h>

class ConfigVal {
private:
    int64_t iVal;       // Valor num�rico positivo/negativo.
    uint64_t uiVal;     // Valor num�rico positivo
    std::string strVal; // Valor de texto.
public:
    ConfigVal();
    ConfigVal(ConfigVal &other);        // Copy
    ConfigVal(std::stringstream &sstr); // Copy
    ~ConfigVal();

    /*
    * @brief: Tipos de dato usados por la clase.
    */
    enum DataTypes{
        dtInt64,    // int64_t
        dtUint64,   // uint64_t
        dtString    // std::string
    };

    /*
    * @brief Devuelve el tipo de dato almacenado.
    */
    DataTypes getDataType();
    int64_t getInt();
    uint64_t getUint();
    std::string getString();
    bool isEmpty();

    ConfigVal operator=(ConfigVal &other);
    operator bool() { return getInt() == 0 ? false : true; };
    bool operator =(bool x){ 
        iVal = x ? 1 : 0; 
        return x;
    };

    operator int8_t(){ return (int8_t)getInt(); };
    int8_t operator =(int8_t x) { 
        iVal = x;
        return x;
    };

    operator uint8_t() { return (uint8_t)getInt(); };
    uint8_t operator =(uint8_t x) {
        iVal = x;
        return x;
    };

    operator int32_t() { return (int32_t)getInt(); };
    int32_t operator =(int32_t x) {
        iVal = x;
        return x;
    };

    operator uint32_t() { return (uint32_t)getInt(); };
    uint32_t operator =(uint32_t x) {
        iVal = x;
        return x;
    };

    operator int64_t() { return iVal; };
    int64_t operator =(int64_t x) {
        iVal = x;
        return x;
    };

    operator uint64_t() { return uiVal; };
    uint64_t operator =(uint64_t x) {
        uiVal = x;
        return x;
    };

    operator std::string(){ return strVal; };
    std::string operator =(std::string x){
        strVal = x;
        return x;
    };

    std::stringstream &operator =(std::stringstream x){
        int64_t i64Test;
        uint64_t ui64Test;
        std::string strTest;
        if (x >> i64Test){
            (*this) = i64Test;
            return x;
        }
        else if (x >> ui64Test){
            (*this) = ui64Test;
            return x;
        }
        else if (x >> strTest){
            (*this) = strTest;
            return x;
        }
        return x;
    };
};