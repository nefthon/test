#pragma once

#include <map>
#include "configVal.h"
#include "configKey.h"

/*
* @brief Clase que guarda las claves y los valores de cada setting.
*/
extern class ConfigManager : public std::map<cfgKeys, ConfigVal>{
public:
    ConfigManager();
    ~ConfigManager();
    /*
    * @brief Intenta inicializar el ConfigManager.
    * Genera todas las claves con sus valores por defecto y luego intenta sobreescribirlas con los valores que haya
    * en el archivo #CONFIGFILE, generando uno nuevo con los valores default en caso de no poder leerlo.
    * return false en caso de ser imposible tanto leer el archivo como crear uno nuevo.
    */
    bool init();

    /*
    * @brief Genera claves y valores por defecto.
    */
    void setupDefaults();

    /*
    * @brief A�ade una clave y un valor.
    * @param key Clave a a�adir.
    * @param val Valor de la clave.
    */
    void addKey(std::string key, ConfigVal &val);

    /*
    * @brief A�ade una clave y un valor.
    * @param key Clave a a�adir.
    * @param val Valor de la clave.
    */
    void addKey(cfgKeys key, ConfigVal &val);

    /*
    * @brief Devuelve el valor de una clave.
    * @param key Clave a devolver.
    * return Valor de la clave.
    */
    ConfigVal &getVal(cfgKeys key);

    /*
    * @brief Devuelve el valor de una clave.
    * @param key Clave a devolver.
    * return Valor de la clave.
    */
    ConfigVal &getVal(std::string key);
    
    /*
    * @brief Asigna un valor nuevo a una clave existente.
    * @param key Clave a cambiar.
    * @param val Valor a asignar.
    */
    void setVal(cfgKeys key, ConfigVal & val);

    /*
    * @brief Devuelve el valor del enumerador de claves del string proporcionado.
    * @return enum.
    */
    cfgKeys getEnumKey(std::string k);


    /*
    * @brief Comprueba si la Clave proporcionada es valida.
    * @return true / false.
    */
    bool isValidKey(cfgKeys k);

    /*
    * @brief Comprueba si la Clave proporcionada es valida.
    * @return true / false.
    */
    bool isValidKey(std::string k);
} gCfg ;