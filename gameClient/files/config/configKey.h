#pragma once

/*
* @brief Enumerador con las claves de configuraci�n.
* Nota: Hay que a�adir una entrada a cfgStrings[] con el texto del mismo y una entrada en ConfigManager::setupDefaults() con el valor por defecto.
*/
enum cfgKeys{
    ScreenSizeX,    //int
    ScreenSizeY,    //int
    FPSLimit,       //int
    GridEnabled,    //int
    cfgKeys_QTY
};

/*
* @brief String con las claves de configuraci�n.
* Nota: Hay que a�adir una entrada a cfgKeys con el id de la clave y una entrada en ConfigManager::setupDefaults() con el valor por defecto.
*/
static std::string cfgStrings[cfgKeys_QTY]{
    "ScreenSizeX",
    "ScreenSizeY",
    "FPSLimit",
    "GridEnabled"
};