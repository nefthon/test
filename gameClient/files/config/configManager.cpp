#include "configManager.h"
#include "configKey.h"
#include "configVal.h"
#include "ConfigFile.h"

#include <logger/logger.h>

ConfigManager::ConfigManager() {
}

ConfigManager::~ConfigManager() {
    ConfigFile::writeAll();
}

bool ConfigManager::init() {
    if (ConfigFile::readAll()){
        return true;
    }
    else{
        _ERRORLOG("Archivo de configuracion no encontrado.");
        _EVENTLOG("Generando archivo de configuracion.");
        if (ConfigFile::writeAll()){
            return true;
        }
        else{
            _ERRORLOG("Imposible crear archivo de configuracion, cerrando aplicacion ...");
            return false;
        }
    }
}

void ConfigManager::setupDefaults(){
    for (int i = 0; i < cfgKeys_QTY; i++){
        ConfigVal val;          // Valor por defecto...
        switch ((cfgKeys)i){    // ... para cada clave.
            case ScreenSizeX:
                val = 1920;
                break;
            case ScreenSizeY:
                val = 1080;
                break;
            case FPSLimit:
                val = 60;
                break;
            case GridEnabled:
                val = 1;
                break;
            default:    // en caso de no estar definida la clave se generar� un error dentro de addKey();
                break;
        }
        addKey(cfgKeys(i), val);
    }
}

void ConfigManager::addKey(std::string key, ConfigVal &val){
    addKey(getEnumKey(key), val);
}

void ConfigManager::addKey(cfgKeys key, ConfigVal & val){
    if (!isValidKey(key)){
        _ERRORLOG("addKey: Clave '" << key << "'invalida.")
            return;
    }
    if (val.isEmpty()){
        _ERRORLOG("addKey: Clave '" << key << "' sin valor.");
        return;
    }
    (*this)[key] = val;
}

ConfigVal &ConfigManager::getVal(cfgKeys key){
    return (*this)[key];
}

ConfigVal &ConfigManager::getVal(std::string key){
    return getVal(getEnumKey(key));
}

void ConfigManager::setVal(cfgKeys key, ConfigVal & val){
    if (isValidKey(key)){
        (*this)[key] = val;
    }
}

cfgKeys ConfigManager::getEnumKey(std::string key){
    for (uint8_t i = 0; i < cfgKeys_QTY; i++){
        if (_strcmpi(cfgStrings[i].c_str(), key.c_str()) == 0)
            return (cfgKeys)i;
    }
    return cfgKeys_QTY;
}

bool ConfigManager::isValidKey(cfgKeys k){
    return k < cfgKeys_QTY;
}

bool ConfigManager::isValidKey(std::string k){
    return isValidKey(getEnumKey(k));
}