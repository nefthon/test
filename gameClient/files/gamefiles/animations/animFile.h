#pragma once
#include <sstream>
#include "../file.h"
#include "../idxReader.h"
#include "animation.h"
#include <logger/logger.h>

class AnimFile {
public:
    enum FileID {
        Anim1,
        Anim2,
        Anim3,
        Anim4,
        Anim5,
        Anim_QTY
    };
    std::string getName(int fileid, bool isIndexFile = false) {
        stringstream name;
        switch ((FileID)fileid) {
            case Anim1:
                name << "anim";
                break;
            case Anim2:
                name << "anim2";
                break;
            case Anim3:
                name << "anim3";
                break;
            case Anim4:
                name << "anim4";
                break;
            case Anim5:
                name << "anim5";
                break;
            default:
                return "";
        }
        if (isIndexFile) {
            name << ".idx";
        }
        else {
            name << ".mul";
        }
        return name.str();
    }
private:
    File file[Anim_QTY];
    std::vector<IdxEntry> animIdx[5];
public:

    AnimFile();
    ~AnimFile();
    void init();
    void readAnimIndex(int32_t id);
};