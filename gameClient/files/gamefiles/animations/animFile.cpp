#include "animFile.h"
#include "animation.h"
#include <logger/logger.h>
#include "../idxReader.h"

AnimFile::AnimFile() {

}

AnimFile::~AnimFile() {

}

void AnimFile::init(){
    File curAnimMul;
    for (int i = 0; i < Anim_QTY; i++) { // do a loop per each anim.idx/anim.mul
        curAnimMul.open(getName(i).c_str());
        if (!curAnimMul.is_open()) {
            continue;
        }
        readAnimIndex(i);
    }
    curAnimMul.close();
}

void AnimFile::readAnimIndex(int32_t id){
    IdxReader curAnimIdx;
    curAnimIdx.open(getName(id, true).c_str()); // Open the anim.idx file
    if (!curAnimIdx.is_open()) {
        return;
    }
    uint32_t maxIndex = curAnimIdx.getSize() / 12;  // max file size.
    uint32_t index = 0;  // starting frame index.
    std::vector<IdxEntry> animList;   // vector storing all frames inside this *.idx file.
    while (index < maxIndex) {
        IdxEntry idxVal;       // filling FrameIndex struct with the incoming data.
        idxVal = curAnimIdx.getIndex(index);
        animList.push_back(idxVal);  // Storing the data in this file's vector.
        index++;
    }
    animIdx[id] = animList;    // adding this file's vector to the main storage vector.
    curAnimIdx.close();
}



