#include <logger/logger.h>
#include "bodyConv.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
using namespace std;

BodyConv::BodyConv(){
    list = new BodyConversion[2048];
    memset(reinterpret_cast<char*>(list), 0, sizeof(BodyConversion) * 2048);
    init();
}

BodyConv::~BodyConv(){
}

void BodyConv::init() {
    openFile("Bodyconv.def");
    if (!is_open()) {
        _ERRORLOG("Imposible abrir Bodyconv.def.");
        return;
    }
    _EVENTLOG("File size = " << getSize());
    string *key = new string[5];        // Bodyconv.def has 5 valid key values, if more are found they get ignored.
    while (!eof()) {
        if (eof())
            break;
        key->clear();
        for (uint8_t currentKey = 0; currentKey < 5; currentKey++) {
            key[currentKey] = getVal();
            if (key[0].c_str() == static_cast<char const*>(nullptr))
                break;
        }
        if (key[0].size() > 0) {
            BodyConversion bc;
            bc.body = (int16_t)atoi(key[0].c_str());
            bc.anim2 = (int16_t)atoi(key[1].c_str());
            bc.anim3 = (int16_t)atoi(key[2].c_str());
            bc.anim4 = (int16_t)atoi(key[3].c_str());
            bc.anim5 = (int16_t)atoi(key[4].c_str());
            list[bc.body] = bc;
        }
    }
}

AnimFileID BodyConv::getAnimFileId(uint16_t body) {
    if (list[body].body > 0) {  // No entry = no conversion.
        if (list[body].anim2 > 0) {
            return Anim2;
        }
        else if (list[body].anim3 > 0) {
            return Anim3;
        }
        else if (list[body].anim4 > 0) {
            return Anim4;
        }
        else if (list[body].anim5 > 0) {
            return Anim5;
        }
    }
    return Anim1;
}

uint16_t BodyConv::getAnimIndex(uint16_t body) {
    if (list[body].body > 0) {  // No entry = no conversion.
        if (list[body].anim2 > 0) {
            return list[body].anim2;
        }
        else if (list[body].anim3 > 0) {
            return list[body].anim3;
        }
        else if (list[body].anim4 > 0) {
            return list[body].anim4;
        }
        else if (list[body].anim5 > 0) {
            return list[body].anim5;
        }
    }
    return body;
}
