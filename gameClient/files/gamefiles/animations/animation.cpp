#include "animation.h"
#include "bodyConv.h"

BodyConv gBodyconv;
Animation::Animation(uint16_t body){
    _body = body;
    uint8_t size = getAnimFrameCount();
    frame = new uint32_t*[size];
    for (int i = 0; i < size; i++) {
        frame[i] = new uint32_t[5];
        memset(frame[i], 0, sizeof(uint32_t) * 5);
    }
}

Animation::~Animation(){
    uint8_t frameCount = getAnimFrameCount();
    for (uint8_t curFrame = 0; curFrame < frameCount; curFrame++) {
        delete frame[curFrame];
    }
}

uint32_t Animation::getAnim(uint8_t action, uint8_t dir) {
    if (frame[action][dir] == 0) {

    }
    return frame[action][dir];
}

AnimFrameType Animation::getAnimFrameType() {
    switch (getAnimFileId()) {
        case Anim5:
            if (_body < 200)
                return AnimMed;
            if (_body < 400)
                return AnimLow;
            return AnimHigh;
        case Anim4:
            if (_body < 200)
                return AnimMed;
            if (_body < 400)
                return AnimLow;
            return AnimHigh;
        case Anim3:
            if (_body < 300)
                return AnimLow;
            if (_body < 400)
                return AnimMed;
            return AnimHigh;
        case Anim2:
            if (_body < 200)
                return AnimMed; //high
            return AnimLow; //low
        case Anim1:
        default:
            if (_body < 200)
                return AnimMed; //high
            if (_body < 400)
                return AnimLow; //low
            return AnimHigh; //people
    }
}

uint8_t Animation::getAnimFrameCount() {
    switch (getAnimFrameType()) {
        case AnimHigh:
            return 35;
        case AnimMed:
            return 22;
        case AnimLow:
        default:
            return 13;
    }
}

AnimFileID Animation::getAnimFileId() {
    return gBodyconv.getAnimFileId(_body);
}

uint16_t Animation::getAnimConvertedBody(){
    return gBodyconv.getAnimIndex(_body);
}

uint32_t Animation::getAnimFrameIndex(uint8_t action, uint8_t direction) {
    int32_t index;
    int32_t lenght = (getAnimFrameCount() * 5) + 1;
    switch (getAnimFileId()) {
        case Anim5:
            if ((_body < 200) && (_body != 34)) // looks strange, though it works.
                index = _body * lenght;
            else if (_body < 400)
                index = 22000 + ((_body - 200) * lenght);
            else
                index = 35000 + ((_body - 400) * lenght);
            break;
        case Anim4:
            if (_body < 200)
                index = _body * lenght;
            else if (_body < 400)
                index = 22000 + ((_body - 200) * lenght);
            else
                index = 35000 + ((_body - 400) * lenght);
            break;
        case Anim3:
            if (_body < 300)
                index = _body * lenght;
            else if (_body < 400)
                index = 33000 + ((_body - 300) * lenght);
            else
                index = 35000 + ((_body - 400) * lenght);
            break;
        case Anim2:
            if (_body < 200)
                index = _body * lenght;
            else
                index = 22000 + ((_body - 200) * lenght);
            break;
        case Anim1:
        default:
            if (_body < 200)
                index = _body * lenght;
            else if (_body < 400)
                index = 22000 + ((_body - 200) * lenght);
            else
                index = 35000 + ((_body - 400) * lenght);
            break;
    }

    index += action * 5;

    if (direction <= 4)
        index += direction;
    else
        index += direction - (direction - 4) * 2;
    return index;
}
