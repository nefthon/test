#pragma once
#include <stdint.h>
// Animation reading anim.idx > anim.mul:

enum AnimFrameType {
    AnimLow,
    AnimMed,
    AnimHigh
};

enum AnimFileID {
    Anim1,
    Anim2,
    Anim3,
    Anim4,
    Anim5,
    Anim_QTY
};

class Animation {
private:
    uint16_t _body;
    uint32_t **frame;
public:
    Animation(uint16_t body);
    ~Animation();

    uint32_t getAnim(uint8_t action, uint8_t dir);
    AnimFrameType getAnimFrameType();
    uint8_t getAnimFrameCount();
    AnimFileID getAnimFileId();
    uint16_t getAnimConvertedBody();
    uint32_t getAnimFrameIndex(uint8_t action, uint8_t direction);
};

// Anim.mul: raw data
/*
struct Animation {      ///< Main block of Animation data.
    int16_t palette[0x100];         ///< Color palette [0x100 * 2b = 512b], used in pixelPalette to fill each pixel with the required color.
    int32_t frameCount;             ///< Number of frames this animation have (Mostly 8 frames per animation: walk, run, etc).
    int32_t lookupTable[frameCount];///< Lookup of each Frame. int * lookupTable = new int[framecount]; read((char*)lookupTable, sizeof(int) * framecount);
    FrameData frameData;            ///< Frames. for (int i = 0; i < frameCount; i++) { int32_t index = lookupTable[i]; // Starting position of this frame }
};

struct FrameData {      ///< Main block of Frame data.
    int16_t centerX;                ///< Center position (X relative).
    int16_t centerY;                ///< Center position (Y relative).
    int16_t width;                  ///< Width of the frame.
    int16_t height;                 ///< Height of the frame.
    PixelData pixelData;            ///< Raw data of pixels, size = width * height.
};

struct PixelData {      ///< Main block of chunks of pixel data.
    int32_t header;                 ///< Id of this pixel, header = 0x7FFF7FFF = chunk done.*
    uint8_t pixelPalette[header.XRun];   ///< 
};

*PixelData.header types
XRun        0B	0A	09	08	07	06	05	04	03	02	01	00
XOffset     15	14	13	12	11	10	0F	0E	0D	0C
YOffset     1F	1E	1D	1C	1B	1A	19	18	17	16

*

header must be decomposed into:
int xOffset = (header � 22) & 0x000003FF;
    xOffset = (XOffset ^ 0x200) - 0x200;
int yOffset = (header � 12) & 0x000003FF;
    yOffset = (YOffset ^ 0x200) - 0x200;
int XRun = header & 0x00000FFF;



***PixelData code
int32_t header = readInt();
uint32_t *imageStream = new uint[width * height];
memset(imageStream, 0, sizeof(uint32_t) * width * height);
while ( header != 0x7FFF7FFF){
    uint32_t px = centerX, py = centerY;
    int xoffset = (header >> 22) & 0x000003FF,
    yoffset = (header >> 12) & 0x000003FF;
    xoffset = (xoffset ^ 0x200) + 0x200;
    yoffset = (yoffset ^ 0x200) + 0x200;
    px += xoffset;
    py += yoffset;
    int XRun = header & 0x00000FFF;
    for (int i = 0; i < xRun; i++){
        byte color = readChar();
        imageStream[py * w + px + i] = color16_to_color32_gray(palette[color]);
    }
    header = readInt();
}



extern float default_color[4];

#define color16_to_color32(x) (unsigned int)(((((x) >> 10) & 0x1F) * 0xFF / 0x1F) | (((((x) >> 5) & 0x1F) * 0xFF / 0x1F) << 8) | ((((x) & 0x1F) * 0xFF / 0x1F) << 16))
#define is_gray(x) (bool)(((x) & 0x1F) == (((x) >> 5) & 0x1F) && ((x) & 0x1F) == (((x) >> 10) & 0x1F))
#define color16_to_color32_gray(x)  (((unsigned int)((x) >> 10) & 0x0000001F) | 0x80000000)
#define get_huetable_lookup(x)  (((unsigned short)((x) >> 10) & 0x001F))

void color32_to_fv4(unsigned int c, float * v);
*/

