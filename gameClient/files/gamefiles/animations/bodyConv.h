#pragma once
#include "../file.h"
#include "animation.h"

struct BodyConversion {
    uint16_t body;
    uint16_t anim2;
    uint16_t anim3;
    uint16_t anim4;
    uint16_t anim5;
    BodyConversion() { body = anim2 = anim3 = anim4 = anim5 = 0; }
    ~BodyConversion(){}
};

extern class BodyConv : public File {
private:
    BodyConversion *list;
public:
    BodyConv();
    ~BodyConv();
    void init();
    AnimFileID getAnimFileId(uint16_t body);
    uint16_t getAnimIndex(uint16_t body);
} gBodyconv;