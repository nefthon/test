#pragma once
#include <iostream>
#include <fstream>
using namespace std;

class File : public ifstream {
public:
    File();
    ~File();

    /*
    * @brief Open the file with the given path and flags.
    */
    bool openFile(const char* file, int32_t flags = ios::in);
    /*
    * @brief Open the file with the given path in binary mode.
    */
    bool openBinary(const char* file);
    /*
    * @brief Returns the next value of the requested type.
    */
    int8_t getInt8();
    /*
    * @brief Returns the next value of the requested type.
    */
    uint8_t getUint8();
    /*
    * @brief Returns the next value of the requested type.
    */
    int16_t getInt16();
    /*
    * @brief Returns the next value of the requested type.
    */
    uint16_t getUint16();
    /*
    * @brief Returns the next value of the requested type.
    */
    int32_t getInt32();
    /*
    * @brief Returns the next value of the requested type.
    */
    uint32_t getUint32();
    /*
    * @brief Returns the next value of the requested type.
    */
    char getChar();
    /*
    * @brief Returns the size of the file in bytes.
    */
    uint32_t getSize();

    std::string getVal();
};