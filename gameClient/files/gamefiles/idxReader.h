#pragma once
#include "file.h"

// Anim.idx: index of animations.

//index * 12 gives the position of the 'index' animation, eg: 0x1000 * 12 = start of the animation 0x1000
//Read 4 bytes 3 times to get 3 ints, which represents:
struct IdxEntry {
    int32_t lookup; ///< Starting position of this animation block in the corresponding anim(x).mul file.
    int32_t length; ///< Lenght of the animation.
    int32_t extra;  ///< Animations do not rely on this field, so it will be 0 (mostly?).
    IdxEntry() { lookup = length = extra = 0; }
    ~IdxEntry() {}
};

class IdxReader : public File {
public:
    IdxReader();
    ~IdxReader();

    IdxEntry getIndex(int32_t body);
};