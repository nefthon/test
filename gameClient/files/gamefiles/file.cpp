#include "file.h"
#include <sstream>
#include <logger/logger.h>

File::File(){
}

File::~File(){
}

bool File::openBinary(const char * file){
    return openFile(file, ios::binary | ios::in);
}

bool File::openFile(const char * file, int32_t flags) {
    stringstream fileRoot;
    fileRoot << "../media/" << file;
    open(fileRoot.str().c_str(), flags);
    //_DEBUGLOG("Abriendo archivo: " << file << " ...");

    if (is_open()) {
        //_DEBUGLOG("... archivo abierto.");
        return true;
    }
    fileRoot.clear();

    fileRoot << "../../media/" << file;
    open(fileRoot.str().c_str(), ios::binary | ios::in);
    if (is_open()) {
        //_DEBUGLOG("... archivo abierto.");
        return true;
    }
    else {
        _ERRORLOG("Error abriendo el archivo " << file);
        return false;
    }
}

int8_t File::getInt8(){
    int8_t v = 0;
    read((char *)&v, sizeof(int8_t));
    return v;
}

uint8_t File::getUint8(){
    uint8_t v = 0;
    read((char *)&v, sizeof(uint8_t));
    return v;
}

int16_t File::getInt16(){
    int16_t v = 0;
    read((char *)&v, sizeof(int16_t));
    return v;
}

uint16_t File::getUint16(){
    uint16_t v = 0;
    read((char *)&v, sizeof(uint16_t));
    return v;
}

int32_t File::getInt32(){
    int32_t v = 0;
    read((char *)&v, sizeof(int32_t));
    return v;
}

uint32_t File::getUint32() {
    uint32_t v = 0;
    read((char *)&v, sizeof(uint32_t));
    return v;
}

char File::getChar(){
    char v;
    read(&v, 1);
    return v;
}

uint32_t File::getSize(){
    seekg(0, ios::end);
    uint32_t size = (uint32_t)tellg();
    seekg(0, ios::beg);
    return size;
}

std::string File::getVal() {
    if (eof())
        return "";
    char chr = 0;
    char lastChr = 0;
    char tmpChr = 0;
    bool lineComment = false;       // true = a comment is in proccess, ignore everything in this line.
    bool multilineComment = false;  // true = a multiline comment is in proccess, ignore everything until it's end '*/'
    stringstream lineStream;        // storage for valid data readed, emptied for found keyword.
    while (get(chr) && !eof()) {
        lastChr = tmpChr;
        tmpChr = chr;
        switch (chr) {
            case '#':
                lineComment = true;
                continue;   // Comment start, keep reading until EOL.
            case '/':
                if (lastChr == '/') {   // '//'
                    lineComment = true;
                    continue;   // Comment start, keep reading until EOL.
                }
                if (lastChr == '*') {   // '*/'
                    multilineComment = false;
                    continue;   // MultiLine comment end, keep reading until EOL.
                }
            case '*':
                if (lastChr == '/') {
                    multilineComment = true;    // MultiLine comment start, keep reading until it's end.
                }
                continue;
            case '=':
            case ':':
            case ' ':
            case ',':
                break;
            case 10:    // EOL
                lineStream.clear();
                lineComment = false;
                break;
            case '"':
                continue;
            default:
            {
                if (lastChr == '\'' ){
                    if (chr == '0' || chr == 'n' || chr == 'c') {    // '\0'
                        lineComment = false;
                        lineStream.clear();
                        if (!multilineComment) {
                            break;  // EOL, break switch and read line.
                        }
                    }
                }
                if (lineComment || multilineComment) {
                    continue;
                }
                if (chr == 255) {
                    lineStream << 0;
                    break;
                }
                if (chr >= 0 && chr <= 255 && (isdigit(chr) || isalpha(chr))) { // legit char (numer or letter)...
                    lineStream << chr;  // store it.
                    continue;
                }
            }
        }
        if (lineStream.str().length() > 0) {
            return lineStream.str(); // store current key.
        }
    }
    return lineStream.str();
}
