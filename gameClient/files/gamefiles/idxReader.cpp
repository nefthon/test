#include "idxReader.h"
#include <logger/logger.h>

IdxReader::IdxReader(){
}

IdxReader::~IdxReader(){
}

IdxEntry IdxReader::getIndex(int32_t body){
    seekg(body * 12, ios::beg);  // Each frame's information has  3 * 4 bytes.
    IdxEntry frameIndex;
    frameIndex.lookup = getInt32();
    frameIndex.length = getInt32();
    frameIndex.extra = getInt32();
    return frameIndex;
}
