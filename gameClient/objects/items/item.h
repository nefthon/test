#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "../object.h"

class Item : public Object {
public:
    Item();
    ~Item();
};