#pragma once

#include "object.h"

class Grid;

class ObjectMovable : public Object{
private:
public:
    ObjectMovable();
    ~ObjectMovable();
    void move(unsigned int x, unsigned int y);  // Se mueve X,Y pixeles dentro del grid
    void moveToGrid(Grid *nuevoGrid);           // Entra en un Grid nuevo, ya sea por crearse en �l o por avanzar y dejar el Grid anterior.
};