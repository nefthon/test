
set( gc_objects_srcs 
	${CMAKE_CURRENT_LIST_DIR}/object.h
	${CMAKE_CURRENT_LIST_DIR}/object.cpp
	${CMAKE_CURRENT_LIST_DIR}/objectMovable.h
	${CMAKE_CURRENT_LIST_DIR}/objectMovable.cpp)
SOURCE_GROUP (objects FILES ${gc_objects_srcs})

include(${CMAKE_CURRENT_LIST_DIR}/units/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/items/CMakeLists.txt)