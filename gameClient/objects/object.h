#pragma once

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

class Grid;

class Object : public sf::Sprite {
protected:
    std::pair<unsigned int, unsigned int> _pos; //Posicion dentro del grid
    Grid *grid;
public:
    Object();
    ~Object();
};