#include "objectMovable.h"
#include <logger/logger.h>
#include "../map/grid.h"
#include "../map/map.h"

ObjectMovable::ObjectMovable() {
}

ObjectMovable::~ObjectMovable() {
}


void ObjectMovable::move(unsigned int x, unsigned int y) {
    unsigned int newX = _pos.first + x,
        sizeX = grid->getSize().x,
        newY = _pos.second + y,
        sizeY = grid->getSize().y;
    if (newX > sizeX) {
        Grid *newGrid = (gMap)[std::make_pair(grid->getMapPos().first + 1, grid->getMapPos().second)];
        if (newGrid) {
            moveToGrid(newGrid);
            move(newX - sizeX, newY);
        }
        else {
            _ERRORLOG("Intentando mover fuera de los l�mites");
            return;
        }
    }
    if (newY > sizeY) {
        Grid *newGrid = (gMap)[std::make_pair(grid->getMapPos().first, grid->getMapPos().second + 1)];
        if (newGrid) {
            moveToGrid(newGrid);
            move(newX, newY - sizeY);
        }
        else {
            _ERRORLOG("Intentando mover fuera de los l�mites");
            return;
        }
    }
}

void ObjectMovable::moveToGrid(Grid * nuevoGrid) {
    if (!nuevoGrid) {
        _ERRORLOG("Intentando a�adir objeto a un grid inexistente");
        return;
    }
    _pos.first = 0;
    _pos.second = 0;
    if (grid)
        grid->deleteTile(this);
    nuevoGrid->addTile(this);
}
