#include "gameView.h"
#include <SFML/Window/Event.hpp>
#include <logger/logger.h>
#include "../files/config/configManager.h"


GameView::GameView(sf::RenderWindow *window){
    _gameWindow = window;
    // Mapeado
   gMap.init();
    gMap.paint(window);
    _showGrid = false;
}

GameView::~GameView(){
}

void GameView::handleInput(){
    sf::Event ev; 
    static bool windowdrag = false;
    while (_gameWindow->pollEvent(ev)){
        switch (ev.type){
            case sf::Event::Closed:
                _gameWindow->close();
                break;
            case sf::Event::KeyPressed:
            {
                switch (ev.key.code){
                    case sf::Keyboard::Escape:
                        _gameWindow->close();
                        break;
                    case sf::Keyboard::G:
                    {
                        _showGrid = !_showGrid;
                        ConfigVal v;
                        v = _showGrid;
                        gCfg.setVal(GridEnabled, v);
                    }break;
                    default:
                        break;
                }
            }break;
            case sf::Event::MouseButtonPressed:
            {
                switch (ev.mouseButton.button){
                    case sf::Mouse::Button::Left:
                    {
                        windowdrag = true;
                        _mouseoffset = sf::Mouse::getPosition(*_gameWindow);
                    }
                    break;
                    default:
                        break;
                }
            }break;
            case sf::Event::MouseButtonReleased:
            {
                switch (ev.mouseButton.button){
                    case sf::Mouse::Button::Left:
                    {
                        windowdrag = false;
                    }
                    break;
                    default:
                        break;
                }
            }break;
            case sf::Event::MouseMoved:
            {
                if (windowdrag){
                    sf::Vector2f mapPos = gMap.getMapSprite()->getPosition();  // Posicion actual del mapa.
                    sf::Vector2i pxToMove(sf::Mouse::getPosition(*_gameWindow) - _mouseoffset); // Posicion actual del rat�n - ultima posicion del rat�n.

                    mapPos.x += pxToMove.x; // diff X
                    mapPos.y += pxToMove.y; // diff Y

                    _mouseoffset = sf::Mouse::getPosition(*_gameWindow);    // Reset de la �ltima posici�n del rat�n.

                    gMap.getMapSprite()->setPosition(mapPos);       // Nueva pos.
                    gMap.getMiniMapSprite()->setPosition(mapPos);   // Nueva pos.
                    //_map->getGridSprite()->setPosition(mapPos);   // Nueva pos.
                }
            }break;
            default:
                break;
        }
    }
}

void GameView::onTick(uint32_t et){
    _gameWindow->draw(*gMap.getMapSprite());
    if (_showGrid)
        _gameWindow->draw(*gMap.getGridSprite());
    _gameWindow->draw(*gMap.getMiniMapSprite());
    static uint32_t lastTick = 0;
    lastTick += et;
    if (lastTick >= 25) {
        lastTick = 0;
        //GameTick
    }
}

void GameView::gainFocus(){
}

void GameView::loseFocus(){
}
