#pragma once

#include "view.h"
#include "gameView.h"

class ViewsManager {
private:
    GameView *_gameView;
    View *_currentView;
public:
    ViewsManager();
    ~ViewsManager();
    GameView *getGameView();
    void setGameView(GameView *view);

    View *getCurrentView();
    void setCurrentView(View *view);

};