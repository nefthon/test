
set( gc_views_srcs 
	${CMAKE_CURRENT_LIST_DIR}/view.h
	${CMAKE_CURRENT_LIST_DIR}/view.cpp
	${CMAKE_CURRENT_LIST_DIR}/gameView.h
	${CMAKE_CURRENT_LIST_DIR}/gameView.cpp
	${CMAKE_CURRENT_LIST_DIR}/viewsManager.h
	${CMAKE_CURRENT_LIST_DIR}/viewsManager.cpp)
SOURCE_GROUP (views FILES ${gc_views_srcs})

