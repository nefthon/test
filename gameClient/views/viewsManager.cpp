#include "viewsManager.h"

ViewsManager::ViewsManager(){
    _gameView = nullptr;
    _currentView = nullptr;
}

ViewsManager::~ViewsManager(){
    delete _gameView;
}

GameView * ViewsManager::getGameView(){
    return _gameView;
}

void ViewsManager::setGameView(GameView * view){
    if (_gameView != nullptr)
        delete _gameView;
    _gameView = view;
}

View * ViewsManager::getCurrentView(){
    return _currentView;
}

void ViewsManager::setCurrentView(View * view){
    if (_currentView != nullptr)
        _currentView->loseFocus();
    _currentView = view;
    _currentView->gainFocus();
}
