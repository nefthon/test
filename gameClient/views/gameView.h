#pragma once

#include "view.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include "../map/map.h"

class GameView : public View {
private:
    sf::RenderWindow *_gameWindow;
    bool _showGrid;
    sf::Vector2i _mouseoffset;
public:
    GameView(sf::RenderWindow *window);
    ~GameView();
    void handleInput();
    void onTick(uint32_t et);
    void gainFocus();
    void loseFocus();
};