#pragma once
#include <stdint.h>

class View {
public:
    virtual void handleInput() = 0;
    virtual void onTick(uint32_t et) = 0;
    virtual void gainFocus() = 0;
    virtual void loseFocus() = 0;
};