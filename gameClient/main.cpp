#include <logger/log_manager.h>
#include <logger/logger.h>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include "map/map.h"
#include "files/config/configManager.h"
#include "files/config/configFile.h"

#include "views/viewsManager.h"

#include "files/gamefiles/animations/animFile.h"

LogManager gLog;
ConfigManager gCfg;
Map gMap;
void main() {
    if (!gCfg.init())
        return;
    // Ventana de juego.
    sf::RenderWindow *_game = new sf::RenderWindow();
    _game->create(sf::VideoMode(gCfg.getVal(ScreenSizeX), gCfg.getVal(ScreenSizeY)), "uocb", sf::Style::None);
    _game->setFramerateLimit(gCfg.getVal(FPSLimit));
    _game->setPosition(sf::Vector2i(0, 0));
    _game->setVerticalSyncEnabled(true);
    _game->setKeyRepeatEnabled(false);
    _game->setMouseCursorGrabbed(true);
    _game->clear();

    // Gestor de vistas.
    ViewsManager *viewsManager = new ViewsManager();
    viewsManager->setGameView(new GameView(_game));
    viewsManager->setCurrentView(viewsManager->getGameView());
    AnimFile *af = new AnimFile();
    af->init();
    Animation *atest = new Animation(104);
    _EVENTLOG("Animation[" << 104 << "].convertedBody = " << atest->getAnimConvertedBody() << " in file " << atest->getAnimFileId()+1 << " index = " << atest->getAnimFrameIndex(1,1));
    sf::Clock clock;
    clock.restart();
    while (_game && _game->isOpen()) {
        _game->clear();
        gLog.onTick();
        viewsManager->getCurrentView()->handleInput();
        viewsManager->getCurrentView()->onTick(clock.getElapsedTime().asMilliseconds());
        _game->display();
        clock.restart();
    }
    ConfigFile::writeAll();
    delete viewsManager;
}